# hostarch

```bash
# Execute with curl
curl -Lso- https://gitlab.com/hunter/hostarch/raw/main/main.sh | bash

# Execute with Wget
wget -qO- https://gitlab.com/hunter/hostarch/raw/main/main.sh | bash
```
